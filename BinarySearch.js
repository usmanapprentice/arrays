Binary Search
function binarySearch(array) {
		array = array.map(x=>+x);
		let key = array[array.length-1], EndIndex = array[0];
    	let StartIndex = 0,
        //EndIndex = array.length - 1,
        MidIndex,
        element;
    while (StartIndex <= EndIndex) {
        MidIndex = Math.floor((StartIndex + EndIndex) / 2, 10);
        element = array[MidIndex];
        if (element < key) {
            StartIndex = MidIndex + 1;
        } else if (element > key) {
            EndIndex = MidIndex - 1;
        } else {
            return MidIndex;
        }
    }
    return -1;
}
console.log(binarySearch(['10', '1', '2', '4', '8', '16', '31', '32', '64', '77', '99', '32']));
