function MaxFrequency (array) {
	array = array.map(x=>parseInt(x));
	let SortedArray = array.sort(function(a, b){return a-b});
	let count = 1;
	let longest = 0;
	let num = 0
	let len = SortedArray.length-1;
	for (var i = 0; i<len; i += 1) {
		if (SortedArray[i]==SortedArray[i+1]) {
			count +=1;
			// num = SortedArray[i];
		} else if( count>longest){
			longest = count;
			num = SortedArray[i];
			count = 1;
		}
	}
	console.log(`${num}: ${longest} times.`);
	console.log(SortedArray);
}

MaxFrequency(['13', '4', '1','1','1','1','1','1','1','1', '3','3','3','3', '1', '4', '2', '3', '4', '4', '1', '2', '4', '9', '3', '3', '3']);
