function MaxIncSequence (array) {
	// let array = array;
	let count = 1;
	let longest = 0;
	for (var i = array.length - 1; i >= 0; i--) {
		if (array[i]-array[i-1]==1) {
			count +=1;
		} else if( count>longest){
			longest = count;
			count = 1;
		}
	}
	return longest;
}
console.log(MaxIncSequence(['8', '7', '3', '2', '3', '4', '2', '2', '4', '5', '6', '7']));