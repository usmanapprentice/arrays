//Swapping test;
// let a = 5;
// let b = 6;
// [a, b] = [b,a];

// console.log(a);
// console.log(b);

function SelectionSort (array) {
	let Len = array.length - 1;
	for (let i = 0; i <Len; i++) {
		let min = i;
		for (var j = i+1; j < Len; j++) {
			if (array[j]<array[min]) {
				min = j;
			}
		}
		if (min!=i) {
			[array[i], array[min]]=[array[min], array[i]]; 
		}
	}
	return array;
}

console.log(SelectionSort(['6', '3', '4', '1', '5', '2', '6']))
