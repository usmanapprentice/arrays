let x = [1, 2, 3];
let alpha = ['a', 'b', 'c' , 'd', 'e'];
let numeric = [1, 2, 3];
// console.log(x);
// console.log(x.length);
// iterator IN. access to index is possible. Elements of array can be accessed.......
	// for(i in x){
	// 	console.log(x[i]);
	// }

// iterator OF. access to index is not allowed. but elements can be accessed..

	// for(let value of x){
	// 	console.log(value);
	// }

//array.unshift(value) and array.push(value) add the elements to Start an End of array respectively, 
//and return the new length of the array after adding the element. ### more than one element can be added once at a time
	// let len = x.push(4);
	// console.log(len);
	// console.log(x);
	// len = x.unshift(0);
	// console.log(len);
	// console.log(x);
//array.pop() and array.shift remove the elements from End and Start of array respectively. and return the values removed.
//### only one element can be removed at a time.
	// let removedPop = x.pop();
	// console.log(x);
	// console.log(removedPop);
	// removedPop = x.shift();
	// console.log(x);
	// console.log(removedPop);

//Reversing an Array 
	//x = x.reverse();
	//console.log(x);

//Joining elements of an array;
	// let message = [ 'My', 'name', 'is', 'Usman,', 'I', 'am', 'from', 'India.' ];
	// let joinedText = message.join(' ');
	// console.log(joinedText);
	// let TextinArray = joinedText.split(' ');       //Splitting back to array.
	// console.log(TextinArray);

//Concatenating two arrays.
	// let concatenatedArray = alpha.concat(numeric);
	// console.log(concatenatedArray);
	// concatenatedArray = numeric.concat(alpha);
	// console.log(concatenatedArray);
	//There is another method of getting the same result as above. it returns the length of new array. and adds elements to the array given
	// in the first argument.
	//let len = [].push.apply(alpha, numeric);
	//console.log(alpha);
	//console.log(len);

//Array#slice
	//let sliced = alpha.slice(0, 3);
	//console.log(sliced);
	// let slicedFromTillEnd = alpha.slice(2);
	// console.log(slicedFromTillEnd);

//Array#Splice
 	// alpha = ['a', 'b', 'c' , 'd', 'e', 'f'];
 	// let spliced = alpha.splice(1, 3, 'removed');
 	// console.log(spliced);
 	// console.log(alpha);

//ourSplice method ###Prototyping a function;
	// Array.prototype.ourSplice = function(index,  count) {
	// 	let middle = this.slice(index, index+count);
	// 	let right = this.slice(index+count);
	// 	while (this.length>index) {
	// 		this.pop();
	// 	}

	// 	for(i = 2; i < arguments.length; i += 1){
	// 		this.push(arguments[i]);
	// 	}
	// 	[].push.apply(this, right);
	// 	return middle;
	// };
	
	// let ourSplicedArr = alpha.ourSplice(1,2,'removed');
	// console.log(ourSplicedArr);
	// console.log(alpha);	

//Array#indexof ###returns index of given element;
	// let indexArr = ['a', 'b', 'c' , 'd', 'e'];
	// console.log(alpha.indexOf('b'));
	
	//Protyping a function using indexof function to find all the indexed of a given number.
	//returns an Array if found, and -1 instead.   [].indicesof(searchElement: any, fromIndex?: int);
	// Array.prototype.indicesof = function(Search, from) {
	// 	let indexArray = [];
	// 	let lastFound = from || -1;
	// 	let index = 0;
	// 	while (true) {
	// 		index = this.indexOf(Search, lastFound + 1);
	// 		lastFound = index;
	// 		if (index < 0) {
	// 			break;
	// 		}
	// 		indexArray.push(index);
	// 	}

	// 	return ((indexArray == '')?-1:indexArray);
	// };

	// let testArray = [1,2,3,4,1,2,1,2,1,5];
	// let indexArray = testArray.indicesof(1);
	// console.log(indexArray);


//Array#lastIndexof
	// let indexArr = ['a', 'b', 'c' , 'd', 'e', 'b', 'b'];
	// let index = indexArr.lastIndexOf('b');
	// console.log(index);

//Rest operator ...args
	// function testRest (a,b, ...args) {
	// 	let x = a;
	// 	let y = b;
	// 	let arr = args;
	// 	console.log(x);
	// 	console.log(y);
	// 	console.log(arr);
	// }
	// testRest('a', 'b', 'c' , 'd', 'e', 'b');


